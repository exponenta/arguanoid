﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Controller : MonoBehaviour {

    public float speed = 10f;
    public float deltaMove = 4f;
    public ParticleSystem particle;
    public Transform BG;

    private SpriteRenderer bgSprite;
    private float full = 0;

    bool leftKey = false;
    bool rightKey = false;

	void Start () {
        bgSprite = BG.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        if ((leftKey || Input.GetAxisRaw("Horizontal") < 0) &&
            transform.position.x >= -deltaMove)
        {
            transform.Translate(Vector3.left * speed * Time.fixedDeltaTime);
        }

        if((rightKey || Input.GetAxisRaw("Horizontal") > 0) &&
            transform.position.x <= deltaMove)
        {
            transform.Translate(-Vector3.left * speed * Time.fixedDeltaTime);
        }
    }

    public void OnTriggerEnter2D(Collider2D coll)
    {
        ObjectController oc = coll.GetComponent<ObjectController>();
        if (oc != null)
        {
            GameStatus.gameManager.AddScore();
            full = Mathf.Min(1f, full + 0.1f);
            Destroy(oc.gameObject, 0);
            particle.startColor = oc.particleColor;
            bgSprite.color = Color.Lerp(particle.startColor, bgSprite.color, 0.5f);
            BG.localScale = new Vector3(1, full, 1);
            particle.Play();
        }
    }

    public void onPointDown(BaseEventData e)
    {
        var ev = e as PointerEventData;
        if (ev.position.x < 0.5f * Screen.width)
            leftKey = true;
        else
            rightKey = true;
    }

    public void onPointUp(BaseEventData e)
    {
        var ev = e as PointerEventData;
        if (ev.position.x < 0.5f * Screen.width)
            leftKey = false;
        else
            rightKey = false;
    }
}
