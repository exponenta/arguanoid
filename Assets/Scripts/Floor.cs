﻿using UnityEngine;
using System.Collections;

public class Floor : MonoBehaviour {

    public ParticleSystem particle;
   
    public void OnTriggerEnter2D(Collider2D coll)
    {
        ObjectController oc = coll.GetComponent<ObjectController>();
        if (oc != null)
        {
            GameStatus.gameManager.GameEnd();
            Destroy(oc.gameObject);
            particle.startColor = oc.particleColor;
            particle.transform.position = coll.transform.position;
            particle.Play();
        }
    }
}
