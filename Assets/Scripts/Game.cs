﻿using UnityEngine;
using System.Collections;

public static class GameStatus
{
    public static bool isPause = false;
    public static Game gameManager = null;

}

public class Game : MonoBehaviour {


    private float oldTimeScale = 0;
    public int score = 0;
    public GameObject pauseText;
    public UnityEngine.UI.Text scoreText;

    void Start () {
        oldTimeScale = Time.timeScale;

        GameStatus.gameManager = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void AddScore()
    {
        score++;
        scoreText.text = score.ToString();
    }

    public void GameEnd()
    {
    }

    public void OnPause()
    {
        GameStatus.isPause = !GameStatus.isPause;
        if (GameStatus.isPause)
            Time.timeScale = 0;
        else
            Time.timeScale = oldTimeScale;
        pauseText.SetActive(GameStatus.isPause);
    }
}
