﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Menu : MonoBehaviour {
    public GameObject connectPanel;
    public InputField IPField;
    public GameObject Loader;
    public int serverPort = 7777;

    private NetworkManager manager;
    private float oldTimeScale = -1;

    public void Start()
    {
        if (oldTimeScale < 0)
            oldTimeScale = Time.timeScale;
        if (Time.timeScale == 0)
            Time.timeScale = oldTimeScale;

        manager = GameObject.Find("gameManager").GetComponent<NetworkManager>();
    }
    public void OnPlay()
    {
        manager.networkPort = serverPort;
        manager.StartHost();
    }

    public void OnConnectShow()
    {
        connectPanel.SetActive(true);
    }

    IEnumerator Connecting()
    {
        manager.networkAddress = IPField.text;
        manager.networkPort = serverPort;
        manager.StartClient();
        //connectPanel.SetActive(false);
        Loader.SetActive(true);
        yield return new WaitForSeconds(manager.connectionConfig.ConnectTimeout * 0.001f);
        Loader.SetActive(false);
    }
     
    public void OnConnectClick()
    {
        StartCoroutine(Connecting());
    }

    public void OnExit()
    {
        Application.Quit();
    }
}
