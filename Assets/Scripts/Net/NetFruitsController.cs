﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class NetFruitsController : NetworkBehaviour {

    public delegate void SpavnClack(Vector3 pos);
     
    [SyncVar]
    public float speed = 0;
    public float interpolateFactor = 5f;
    public Transform clackPrefab;

    [SyncVar(hook = "SetNewPosition")]
    private Vector3 myPos;

    [SyncEvent]
    private event SpavnClack EventSpavnClack;

    void Start()
    {
        myPos = transform.position - Vector3.up;

        //if (!isServer)
            EventSpavnClack += SendSpavnClack;
    }
	void FixedUpdate () {
        if (isServer)
        {
            Move();
        }
        else {
            Interpolate();
        }
    }


    void SendSpavnClack(Vector3 pos)
    {
        Vector3 _pos = pos;
        _pos.y = 0.8f;

        Transform p = Instantiate(clackPrefab, _pos, Quaternion.identity) as Transform;
    }

    [Server]
    void Move() {
        transform.Translate(0, -speed * Time.fixedDeltaTime, 0);
        myPos = transform.position;
    }

    [Server]
    void OnTriggerEnter(Collider c)
    {
        if (c.tag == "Player")
            NetGameGlobal.netGame.AddScore();
        else {
            NetGameGlobal.netGame.Missed();
            if (EventSpavnClack != null)
                EventSpavnClack(transform.position);
            SendSpavnClack(transform.position);
        }
        NetworkServer.Destroy(this.gameObject);
        Destroy(this.gameObject);

    }

    [ClientCallback]
    void SetNewPosition(Vector3 pos) {
        if (!isLocalPlayer)
        {
            myPos = pos;
        }
    }

    [ClientCallback]
    void Interpolate()
    {
        transform.position = Vector3.Lerp(transform.position, myPos, interpolateFactor * Time.deltaTime);
    }

}
