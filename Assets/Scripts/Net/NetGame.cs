﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public static class NetGameGlobal
{
    public static NetGame netGame;
    public static bool pause = false;
}

public class NetGame : NetworkBehaviour{

    [SyncVar (hook = "SendGameState")]
    bool myPause = false;
    [SyncVar (hook = "SendScore")]
    int score = 0;

    [SyncVar(hook = "SendMissed")]
    int missed = 0;

    public Text scoreText;
    public GameObject pauseText;

    private float oldTimeScale = 0;

    void Start () {

        NetGameGlobal.netGame = this;

        oldTimeScale = Time.timeScale;
	}
	
    public void AddScore()
    {
        score++;
        scoreText.text = score.ToString();
    }

    public void Missed()
    {
        missed++;
    }

    public void GameEnd()
    {

        Debug.Log("End Invoked");
    }

    public void OnPauseButton()
    {
        NetGameGlobal.pause = !NetGameGlobal.pause;
        myPause = NetGameGlobal.pause;
        if (NetGameGlobal.pause)
            Time.timeScale = 0;
        else
            Time.timeScale = oldTimeScale;
        pauseText.SetActive(NetGameGlobal.pause);
        
    }
    
    public void OnCloseButton()
    {

        Time.timeScale = oldTimeScale;
        if (isServer)
        {
            GameObject.Find("gameManager").GetComponent<NetworkManager>().StopHost();
        }else
        {
            GameObject.Find("gameManager").GetComponent<NetworkManager>().StopClient();
        }
    }

    [Client]
    void SendGameState(bool pause)
    {
        myPause = pause;
        NetGameGlobal.pause = pause;
    }
    
    void SendScore(int s) {
        score = s;
        scoreText.text = score.ToString();
    }

    void SendMissed(int m)
    {
        missed = m;
        scoreText.text = score.ToString();
    }
}
