﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class NetPlayerController : NetworkBehaviour{

    [Range(0,1f)]
    public float inertilaFactor = 0.2f;
    public Transform bucket;
    public float bucketMinFalue = 1f;
    public float bucketMaxFalue = 5f;

    public float rotateSpeed = 10f;
    public float moveSpeed = 10f;

    public float interpolateFactor = 10f;

    [SyncVar (hook ="SendBucketPos")]
    private Vector3 bucketPos;
    [SyncVar(hook = "SendRotation")]
    private Quaternion rotation;

    void Start () {
        bucketPos = bucket.localPosition;
        bucketPos.z = (bucketMinFalue + bucketMaxFalue) * 0.5f;
        bucket.localPosition = bucketPos;
        rotation = transform.rotation;
	}
	
	void FixedUpdate () {
        if (isServer)
        {
            float hor = Mathf.Lerp(Input.GetAxis("Horizontal"), Input.GetAxisRaw("Horizontal"), inertilaFactor);

            hor *= rotateSpeed * Time.fixedDeltaTime;
            float ver = Mathf.Lerp(Input.GetAxis("Vertical"), Input.GetAxisRaw("Vertical"), inertilaFactor);
            ver *= moveSpeed * Time.fixedDeltaTime;
            
            transform.Rotate(Vector3.up, hor);
            rotation = transform.rotation;

            var p = bucket.localPosition;
            p.z = Mathf.Clamp(bucketPos.z + ver, bucketMinFalue, bucketMaxFalue);
            bucketPos = p;
            bucket.localPosition = bucketPos;
        } else
        {
            Interpolate();
        }
	}

    [Client]

    void Interpolate()
    {
        bucket.localPosition = Vector3.Lerp(bucket.localPosition, bucketPos, interpolateFactor * Time.fixedDeltaTime);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, interpolateFactor * Time.fixedDeltaTime);
    }

    void SendBucketPos(Vector3 pos)
    {
        bucketPos = pos; 
    }
    void SendRotation(Quaternion r)
    {
        rotation = r;
    }
}
