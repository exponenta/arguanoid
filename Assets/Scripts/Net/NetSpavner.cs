﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class NetSpavner : NetworkBehaviour {

    public Transform listOfPoints;
    public Transform[] prefabs;

    public float speedDeltaTime = 10f;
    public float speed = 10f;
    public float speedIncrement = 0.1f;
    private float currentSpeed = 10f;

	void Start () {

        currentSpeed = speed;
        StartCoroutine(Spawn());
	}
	
    IEnumerator Spawn()
    {
        yield return new WaitForSeconds(speedDeltaTime);

        int spavnPoint = Random.Range(0, listOfPoints.childCount);
        int prefabIndex = Random.Range(0, prefabs.Length);
        Transform p = (Transform)Instantiate(prefabs[prefabIndex], 
            listOfPoints.GetChild(spavnPoint).transform.position, Quaternion.identity);
        p.GetComponent<NetFruitsController>().speed = currentSpeed;
       // p.SetParent(transform);
        NetworkServer.Spawn(p.gameObject);

        StartCoroutine(Spawn());
    }
	// Update is called once per frame
	void Update () {

	}
}
