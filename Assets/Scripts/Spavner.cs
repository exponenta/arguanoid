﻿using UnityEngine;
using System.Collections;

public class Spavner : MonoBehaviour {

    public Transform[] prefabs;
    public float spawnWidth = 5f;
    public float prefabsStartSpeed = 0.1f;
    public float speedDelta = 0.05f;
    public float spafnPeriod = 1.4f;

    private float prefabSpeed = 0;
    private bool isSpavning = false;

    IEnumerator SpafnPrefab()
    {
        yield return new WaitForSeconds(spafnPeriod);

        int index = Random.Range(0, prefabs.Length);

        Vector3 pos = new Vector3(Random.Range(-spawnWidth * 0.5f, spawnWidth *0.5f), 0, 0);
        Transform p = Instantiate(prefabs[index]) as Transform;
        p.SetParent(transform);
        p.localPosition = pos;
        p.localRotation = Quaternion.AngleAxis(Random.Range(-180, 180), Vector3.forward);

        if (isSpavning)
            StartCoroutine(SpafnPrefab());
    }

    IEnumerator IncrementSpeed()
    {
        yield return new WaitForSeconds(30f);

        prefabSpeed += speedDelta;

        if (isSpavning)
            StartCoroutine(IncrementSpeed());
    }

    void Start () {

        ResetGame();
	}
	
    public void ResetGame()
    {
        prefabSpeed = prefabsStartSpeed;

        isSpavning = true;
        StartCoroutine(IncrementSpeed());
        StartCoroutine(SpafnPrefab());
    }
	// Update is called once per frame
	void Update () {
	
	}
}
